module github.com/imlonghao/imlonghao.com

require (
	github.com/gomarkdown/markdown v0.0.0-20201113031856-722100d81a8e
	github.com/gorilla/feeds v1.1.1
	github.com/kr/pretty v0.2.0 // indirect
	github.com/tdewolff/minify/v2 v2.9.10
)

go 1.15
